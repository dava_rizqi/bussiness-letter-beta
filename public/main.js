var app = new Vue({
    el: '#content',
    data() {
        return {
            company: '',
            address: '',
            company_r: '',
            address_r: '',
            ref: '',
            date: '',
            to: '',
            position: '',
            salutation: '',
            opening: '',
            main: '',
            closing: '',
            writer: '',
            writer_position: '',
            border: true,
        }
    },
    created() {
        var loaded = JSON.parse(localStorage.getItem('letter'))

        if (loaded) {
            this.company = loaded.stored_company;
            this.address = loaded.stored_address;
            this.company_r = loaded.stored_company_r;
            this.address_r = loaded.stored_address_r;
            this.ref = loaded.stored_ref;
            this.date = loaded.stored_date;
            this.to = loaded.stored_to;
            this.position = loaded.stored_position;
            this.salutation = loaded.stored_salutation;
            this.opening = loaded.stored_opening;
            this.main = loaded.stored_main;
            this.closing = loaded.stored_closing;
            this.writer = loaded.stored_writer;
            this.writer_position = loaded.stored_writer_position;
            this.border = loaded.stored_border;
        } else {
            console.warn("can't load letter. Maybe Your First Time here?")
        }
    },
    methods: {
        borderToggle: function () {
            this.border = !this.border
            console.log(this.border)
        },
        saveData: function () {
            var p = {
                stored_company: this.company,
                stored_address: this.address,
                stored_address_r: this.address_r,
                stored_company_r: this.company_r,
                stored_ref: this.ref,
                stored_date: this.date,
                stored_writer: this.writer,
                stored_to: this.to,
                stored_position: this.position,
                stored_salutation: this.salutation,
                stored_opening: this.opening,
                stored_main: this.main,
                stored_closing: this.closing,
                stored_writer_position: this.writer_position,
                stored_border: this.border,
            }
            localStorage.setItem('letter', JSON.stringify(p))
            return p;

        }
    },
    computed: {
        letter() {
            var p = {
                stored_company: this.company,
                stored_address: this.address,
                stored_address_r: this.address_r,
                stored_company_r: this.company_r,
                stored_ref: this.ref,
                stored_date: this.date,
                stored_writer: this.writer,
                stored_to: this.to,
                stored_position: this.position,
                stored_salutation: this.salutation,
                stored_opening: this.opening,
                stored_main: this.main,
                stored_closing: this.closing,
                stored_writer_position: this.writer_position,
                stored_border: this.border,
            }
            localStorage.setItem('letter', JSON.stringify(p))
            return p;
        }
    }
});




function printDoc() {
    var mywindow = window.open('', 'PRINT', 'height=1200,width=700');
    mywindow.document.write(`<html><head><title>` + document.title + `</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                crossorigin="anonymous">
            <link rel="stylesheet" href="style.css">
            <style>
                p {
                    text-align: justify !important;
                }
            </style>
            `);
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById('letter_layout').innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
}